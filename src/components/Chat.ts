// components/Chat.ts
import { Bot } from '../bots/Bot';

export class Chat {
  private container: HTMLElement;
  private bot: Bot | null = null;

  constructor(container: HTMLElement) {
    this.container = container;
  }

  init() {
    this.render();
    this.bindEvents();
  }

  private render() {
    this.container.innerHTML = `
      <div class="chat-window d-none">
        <div id="selectedBot" class="mb-3"></div>
        <div id="messageList" class="message-list"></div>
        <div class="input-group">
          <input id="messageInput" type="text" class="form-control" placeholder="Écrivez un message" disabled>
          <button id="sendButton" class="btn btn-primary" disabled>Envoyer</button>
        </div>
      </div>
    `;
  }

  private bindEvents() {
    const sendButton = document.getElementById('sendButton');
    const messageInput = document.getElementById('messageInput') as HTMLInputElement;
    sendButton?.addEventListener('click', () => this.sendMessage(messageInput.value));
    messageInput?.addEventListener('keypress', (event) => {
      if (event.key === 'Enter') {
        this.sendMessage(messageInput.value);
      }
    });
  }

  public setBot(bot: Bot) {
    this.bot = bot;
    const chatWindow = document.querySelector('.chat-window') as HTMLElement;
    const selectedBot = document.getElementById('selectedBot') as HTMLElement;
    const messageInput = document.getElementById('messageInput') as HTMLInputElement;
    const sendButton = document.getElementById('sendButton') as HTMLButtonElement;
    const messageList = document.getElementById('messageList') as HTMLElement;

    if (chatWindow && selectedBot && messageInput && sendButton && messageList) {
      chatWindow.classList.remove('d-none');
      selectedBot.textContent = `Discussion avec ${bot.name}`;
      messageInput.disabled = false;
      sendButton.disabled = false;
      messageList.innerHTML = ''; // Reset message list on bot change
    }
  }

  private async sendMessage(message: string) {
    if (!message.trim() || !this.bot) return;
    this.addMessage('Vous', message, 'right');
    const response = await this.bot.respond(message);
    this.addMessage(this.bot.name, response, 'left');
    this.clearMessageInput();
  }

  private addMessage(sender: string, message: string, side: 'left' | 'right') {
    const messageList = document.getElementById('messageList');
    if (messageList) {
      this.addMessageToList(messageList, sender, message, side);
    }
  }

  private addMessageToList(messageList: HTMLElement, sender: string, message: string, side: 'left' | 'right') {
    const messageElement = document.createElement('div');
    messageElement.classList.add('message', side === 'right' ? 'text-end' : 'text-start');
    messageElement.innerHTML = `
      <div class="message-content">
        <div class="message-sender">${sender}</div>
        <div class="message-text">${message}</div>
      </div>
    `;
    messageList.appendChild(messageElement);
    messageList.scrollTop = messageList.scrollHeight;
  }

  private clearMessageInput() {
    const messageInput = document.getElementById('messageInput') as HTMLInputElement;
    if (messageInput) {
      messageInput.value = '';
    }
  }
}
