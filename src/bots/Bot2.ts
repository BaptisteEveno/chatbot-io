import { Bot } from './Bot';

export class Bot2 extends Bot {
  constructor() {
    super('Bot2', 'avatar2.png', {
      aide: 'Commandes disponibles : aide, salut, bye, devinette, temps',
      salut: 'Salut ! Comment puis-je t\'aider aujourd\'hui ?',
      bye: 'Bye ! Passe une bonne journée !',
      devinette: 'Pourquoi les ordinateurs ne font-ils jamais de siestes ? Parce qu\'ils ne veulent pas perdre leurs données !',
      temps: 'Le temps est nuageux avec une légère brise.'
    });
  }
}
