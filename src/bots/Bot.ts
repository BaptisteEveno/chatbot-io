// bots/Bot.ts
import axios from 'axios';

export class Bot {
  name: string;
  avatar: string;
  commands: { [key: string]: string };

  constructor(name: string, avatar: string, commands: { [key: string]: string }) {
    this.name = name;
    this.avatar = avatar;
    this.commands = commands;
  }

  async respond(message: string): Promise<string> {
    for (const command in this.commands) {
      if (message.toLowerCase().includes(command)) {
        if (command === 'météo') {
          const city = this.extractCity(message);
          return await this.getWeather(city);
        }
        return this.commands[command];
      }
    }
    return "Je ne comprends pas cette commande.";
  }

  private extractCity(message: string): string {
    const parts = message.split(' ');
    const cityIndex = parts.indexOf('à') !== -1 ? parts.indexOf('à') + 1 : parts.length - 1;
    return parts[cityIndex];
  }

  private async getWeather(city: string): Promise<string> {
    try {
      const apiKey = 'b65112f426c122883de63e507ecd648d';
      const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`);
      const weather = response.data;
      return `Le temps à ${city} est ${weather.weather[0].description} avec une température de ${weather.main.temp}°C.`;
    } catch (error) {
      console.error(error);
      return 'Je ne peux pas obtenir la météo pour le moment.';
    }
  }
}
