import { Bot } from './Bot';

export class Bot1 extends Bot {
  constructor() {
    super('Bot1', 'avatar1.png', {
      aide: 'Commandes disponibles : aide, bonjour, au revoir, blague, météo',
      bonjour: 'Bonjour ! Comment puis-je vous aider aujourd\'hui ?',
      'au revoir': 'Au revoir ! Passez une bonne journée !',
      blague: 'Pourquoi les scientifiques ne font-ils pas confiance aux atomes ? Parce que tout est fait d\'atomes !',
      météo: 'Le temps est ensoleillé avec un léger risque de pluie.'
    });
  }
}
